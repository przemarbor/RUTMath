# RUTMath
<b>R</b>zeszów <b>U</b>niversity of <b>T</b>echnology <b>M</b>ath - is a open source (GPL) application to help learning mathematics for early school children.

# Requirements
To use this app you will need only mobile device using Android Version >= 5.1

# Language
This app is available in the following languages:
- English
- Polish

# License
This software is licensed under the terms of the GNU General Public License version 3 (GPLv3).</br>
Full text of the license is available online https://opensource.org/licenses/gpl-3.0.html

# Screenshots
<table>
<tr>
<td>
       <img width="185px" height="355px" src="https://dl.dropboxusercontent.com/s/l7uk640ixgpxlrr/1.png?dl=0">
</td>
<td>
       <img width="185px" height="355px" src="https://dl.dropboxusercontent.com/s/j6lz1w2yux3z42a/2.png?dl=0">
</td>
<td>
       <img width="185px" height="355px" src="https://dl.dropboxusercontent.com/s/vwzrdi941gz0m9f/3.png?dl=0">
</td>
<td>
       <img width="185px" height="355px" src="https://dl.dropboxusercontent.com/s/3zsgs5qvmo9s44c/4.png?dl=0">
</td>
